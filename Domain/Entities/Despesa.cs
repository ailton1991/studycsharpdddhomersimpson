﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    [Table("Despesa")]
    public class Despesa
    {
        [Column("Id")]
        [Display(Description = "Código")]
        public int       Id              { get; set; }

        [Column("NomeDespesa")]
        [Display(Description = "Nome da Despesa")]
        public string    NomeDespesa     { get; set; }

        [Column("ValorDespesa")]
        [Display(Description = "Valor")]
        public float     ValorDespesa    { get; set; }

        [Column("DataCadastro")]
        public DateTime? DataCadastro    { get; set; }

        [Column("DataDespesa")]
        [Display(Description = "Data da Despesa")]
        public DateTime? DataDespesa     { get; set; }
    }
}
