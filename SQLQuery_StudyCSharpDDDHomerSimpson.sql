-- ####################
-- Inicio - Criar Banco
create database DDDHomerSimpson
go
use DDDHomerSimpson
-- Fim - Criar Banco
-- ####################


-- ####################
-- Inicio - Criar Tabela
GO
create table Despesa(
 Id int primary key identity,
NomeDespesa varchar(255),
ValorDespesa decimal(10,2),
DataCadastro datetime null,
DataDespesa  datetime null
)
-- Fim - Criar Tabela
-- ####################

